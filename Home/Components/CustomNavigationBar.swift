//
//  CustomNavigationBar.swift
//  home
//
//  Created by Yun Zeng on 2019/8/7.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import UIKit

@IBDesignable
class CustomNavigationBar: UIView {
    
    @IBInspectable var icon: UIImage? {
        didSet {
            self.iconView.image = icon
        }
    }
    
    @IBInspectable var title: String = "" {
        didSet {
            let attributes: [NSAttributedString.Key : Any] = [
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.semibold),
                NSAttributedString.Key.foregroundColor: UIColor.black,
                NSAttributedString.Key.kern: -0.26]
            self.titleLabel.attributedText = NSAttributedString(string: title, attributes: attributes)
        }
    }
    
    @IBInspectable var subtitle: String = "" {
        didSet {
            let attributes: [NSAttributedString.Key : Any] = [
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular),
                NSAttributedString.Key.foregroundColor: UIColor(red: 0, green: 0, blue: 0, alpha: 0.6),
                NSAttributedString.Key.kern: 0]
            self.subtitleLabel.attributedText = NSAttributedString(string: subtitle, attributes: attributes)
        }
    }
    
    private lazy var iconView: UIImageView = {
        let view = UIImageView()
        return view
    } ()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        return label
    } ()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        return label
    } ()
    
    private lazy var cancelBtn: UIButton = {
        let btn = UIButton()
        btn.setBackgroundImage(UIImage(named: "cancel-btn-normal"), for: .normal)
        btn.setBackgroundImage(UIImage(named: "cancel-btn-forcus"), for: .focused)
        btn.setBackgroundImage(UIImage(named: "cancel-btn-forcus"), for: .highlighted)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.widthAnchor.constraint(equalToConstant: 30).isActive = true
        btn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        return btn
    } ()
    
    func initialize() {
        self.addSubview(iconView)
        self.addSubview(titleLabel)
        self.addSubview(subtitleLabel)
        self.addSubview(cancelBtn)
        
        self.backgroundColor = UIColor(hex: "EBEBEB")
        
        heightAnchor.constraint(equalToConstant: 72).isActive = true
        
        iconView.translatesAutoresizingMaskIntoConstraints = false
        iconView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15).isActive = true
        iconView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        iconView.widthAnchor.constraint(equalToConstant: 44).isActive = true
        iconView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.leadingAnchor.constraint(equalTo: iconView.trailingAnchor, constant: 6).isActive = true
        titleLabel.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -8).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        titleLabel.autoresizingMask = .flexibleWidth
        
        subtitleLabel.translatesAutoresizingMaskIntoConstraints = false
        subtitleLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor).isActive = true
        subtitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor).isActive = true
        subtitleLabel.heightAnchor.constraint(equalToConstant: 18).isActive = true
        subtitleLabel.autoresizingMask = .flexibleWidth
        
        let divider = UIView()
        self.addSubview(divider)
        divider.backgroundColor = UIColor(hex: "EBEBEB")
        divider.translatesAutoresizingMaskIntoConstraints = false
        divider.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        divider.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        divider.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        divider.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        cancelBtn.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        cancelBtn.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20).isActive = true
        cancelBtn.addTarget(self, action: #selector(handleCancelButton(_:)), for: .touchUpInside)
    }
    
    var cancel: (() -> Void)?
    @objc func handleCancelButton(_ sender: AnyObject) {
        self.cancel?()
        Logger.Info("cancel")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
}




