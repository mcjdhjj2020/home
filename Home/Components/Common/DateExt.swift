//
//  DateExt.swift
//  home
//
//  Created by Yun Zeng on 2019/9/28.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import Foundation

// MARK: - 时间转换
extension Date {
    func toString() -> String {
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        formatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        return formatter.string(from: self)
    }
    
    func toString(format: String) -> String {
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}

extension String {
    func toDate() -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        return formatter.date(from: self)!
    }
}
