//
//  ServiceClock.swift
//  home
//
//  Created by Yun Zeng on 2019/1/31.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import UIKit

// 闹钟服务
class ServiceMusicClock {
    var service: Service
    var timeChar: HMCharacteristic
    var musicChar: HMCharacteristic
    
    init?(service: Service) {
        if service.type != HMServiceType.clock.rawValue {
            return nil
        }
        
        guard let timeChar = service.findCharacteristic(type: .currentTime) else {
            return nil
        }
        
        guard let musicChar = service.findCharacteristic(type: .musicName) else {
            return nil
        }
        
        self.musicChar = musicChar
        self.service = service
        self.timeChar = timeChar
    }
    
    // 显示时间
    var time: String {
        if let value = timeChar.value, let timeStr = value.jsonValue as? String {
            return timeStr
        }
        return ""
    }
}

// 实现单元格显示协议
extension ServiceMusicClock: ServiceCellDelegate {
    var icon: UIView {
        guard  let resource = self.service.icon else {
            return UIView(frame: ServiceCellView.iconSize)
        }
        let imageView = UIImageView(frame: ServiceCellView.iconSize)
        imageView.image = UIImage(named: "service-" + resource)
        return imageView
    }
    
    var room: String {
        if let r = self.service.room {
            return r.name
        }
        return "默认房间"
    }
    
    var name: String {
        if self.service.name != "" {
            return self.service.name
        }
        return "请命名"
    }
    
    var state: Service.State {
        return self.service.state
    }
    
    var desc: String {
        if self.state == .offline {
            return "离线"
        } else if self.state == .update {
            return "正在更细"
        } else {
            return self.time
        }
    }
    
    func toggle() -> HomeAction? {
        return nil
    }
}




