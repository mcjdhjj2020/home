//
//  AutomationCell.swift
//  home
//
//  Created by Yun Zeng on 2019/8/6.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import UIKit

protocol AutomationCellDelegate {
    var title: String {get}
    var desc: String {get}
    var icon: String {get}
}

class AutomationCell: UITableViewCell {
    static let identifier = "automationCellIdentifier"
    
    var automationView: AutomationCellView!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        automationView = AutomationCellView(frame: .zero)
        self.contentView.addSubview(automationView)
        
        self.accessoryType = .disclosureIndicator
        
        automationView.translatesAutoresizingMaskIntoConstraints = false
        automationView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        automationView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        automationView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        automationView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        self.backgroundColor = UIColor.clear
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setAutomation(automation: AutomationCellDelegate) -> Bool {
        var result = false
        if automation.title != self.automationView.name {
            self.automationView.name = automation.title
            result = true
        }
        if automation.desc != self.automationView.desc {
            self.automationView.desc = automation.desc
            result = true
        }
        
        if automation.icon != self.automationView.icon {
            self.automationView.icon = automation.icon
            result = true
        }
        return result
    }
}

extension HomeAutomation: AutomationCellDelegate {
    var desc: String {
        if self.isEnable {
            return self.subtitle
        }
        return "自动化已关闭"    }
    
    var icon: String {
        return "automation-" + self.type
    }
}

