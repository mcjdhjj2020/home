//
//  AutomationCellView.swift
//  home
//
//  Created by Yun Zeng on 2019/8/6.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import UIKit

@IBDesignable
class AutomationCellView: UIView {
    @IBInspectable var icon: String = "" {
        didSet {
            self.iconImage.image = UIImage(named: icon)
        }
    }
    
    @IBInspectable var name: String = "" {
        didSet {
            let attributes: [NSAttributedString.Key : Any] = [
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.regular),
                NSAttributedString.Key.foregroundColor: UIColor(red: 0, green: 0, blue: 0, alpha: 0.8),
                NSAttributedString.Key.kern: 0.32]
            self.titleLabel.attributedText = NSAttributedString(string: name, attributes: attributes)
        }
    }
    
    @IBInspectable var desc: String = "" {
        didSet {
            let attributes: [NSAttributedString.Key : Any] = [
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular),
                NSAttributedString.Key.foregroundColor: UIColor(red: 0, green: 0, blue: 0, alpha: 0.5),
                NSAttributedString.Key.kern: 0.39]
            self.detailLabel.attributedText = NSAttributedString(string: desc, attributes: attributes)
        }
    }
    
    private lazy var iconImage: UIImageView = {
        let view = UIImageView()
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    private lazy var detailLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    func initialize()  {
        self.addSubview(iconImage)
        self.addSubview(titleLabel)
        self.addSubview(detailLabel)
        
        iconImage.translatesAutoresizingMaskIntoConstraints = false
        iconImage.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        iconImage.widthAnchor.constraint(equalToConstant: 40).isActive = true
        iconImage.heightAnchor.constraint(equalToConstant: 40).isActive = true
        iconImage.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.leadingAnchor.constraint(equalTo: iconImage.trailingAnchor, constant: 12).isActive = true
        titleLabel.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -10).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        titleLabel.autoresizingMask = .flexibleWidth
        
        detailLabel.translatesAutoresizingMaskIntoConstraints = false
        detailLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor).isActive = true
        detailLabel.heightAnchor.constraint(equalToConstant: 12).isActive = true
        detailLabel.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 12).isActive = true
        detailLabel.autoresizingMask = .flexibleWidth
        
        self.backgroundColor = UIColor.clear
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
}
