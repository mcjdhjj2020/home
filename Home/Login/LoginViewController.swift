//
//  LoginViewController.swift
//  home
//
//  Created by Yun Zeng on 2019/8/4.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    @IBAction func login(_ sender: UIButton) {
        if let account = self.username.text, let password = self.password.text {
            Logger.Info("login with account(\(account))+password(\(password))")
            _ = Account.instance.login(account: account, password: password)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loginButton.layer.cornerRadius = 8.0
        
        let account = Account.instance
        account.addObserver(identify: "adai.design.login", observer: self)
        if account.membership.account != "" {
            self.username.text = account.membership.account
        }
    }
}

extension LoginViewController: AccountDelegate {
    func AccountDidLogin() {
        Logger.Info("Login")
        self.dismiss(animated: true, completion: nil)
    }
    
    func AccountDidLogout(state: String) {
        Logger.Warning("logout: \(state)")
    }
}
