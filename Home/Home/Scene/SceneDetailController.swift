//
//  SceneDetailController.swift
//  home
//
//  Created by Yun Zeng on 2019/8/7.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import UIKit

class SceneDetailController: UIViewController {
    
    var scene: HomeScene
    private lazy var customBar: CustomNavigationBar = {
        let bar = CustomNavigationBar()
        return bar
    } ()
    
    var onBack: (()->Swift.Void)?
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.onBack?()
    }
    
    init(scene: HomeScene) {
        self.scene = scene
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var actionsView: ActionsCollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(hex: "EDEDED")
        self.customBar.backgroundColor = UIColor(hex: "FFFFFF")
        
        view.addSubview(self.customBar)
        customBar.title = scene.name
        let subtitle = scene.subtitle
        if subtitle != "" {
            customBar.subtitle = subtitle
        } else if scene.state == .activated {
            customBar.subtitle = "所有设备已在状态"
        } else {
            customBar.subtitle = "未执行"
        }

        customBar.icon = UIImage(named: scene.icon + "-on")
        customBar.translatesAutoresizingMaskIntoConstraints = false
        customBar.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        customBar.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        customBar.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        customBar.cancel = {
            self.dismiss(animated: true, completion: nil)
        }
        
        guard let home = HomeManager.instance.current else {
            return
        }
        let manager = ActionsServiceManager(actions: scene.actions, home: home)
        self.actionsView = ActionsCollectionView(frame: .zero, manager: manager, title: "此场景将执行以下动作")
        view.addSubview(actionsView)
    
        actionsView.translatesAutoresizingMaskIntoConstraints = false
        actionsView.topAnchor.constraint(equalTo: customBar.bottomAnchor, constant: 10).isActive = true
        actionsView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        actionsView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        actionsView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        
        let deleteBtn = UIButton()
        deleteBtn.setTitle("删除此场景", for: .normal)
        deleteBtn.setTitleColor(UIColor(red: 1, green: 59/255, blue: 48/255, alpha: 1), for: .normal)
        deleteBtn.setTitleColor(UIColor(red: 1, green: 59/255, blue: 48/255, alpha: 0.6), for: .highlighted)
        deleteBtn.heightAnchor.constraint(equalToConstant: 44).isActive = true
        deleteBtn.layer.cornerRadius = 10
        deleteBtn.backgroundColor = UIColor.white
        view.addSubview(deleteBtn)
        deleteBtn.translatesAutoresizingMaskIntoConstraints = false
        deleteBtn.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        deleteBtn.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10).isActive = true
        deleteBtn.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -64).isActive = true
        
        let editBtn = UIButton()
        editBtn.setTitle("添加或移除配件", for: .normal)
        editBtn.setTitleColor(UIColor(red: 0, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
        editBtn.setTitleColor(UIColor(red: 0, green: 122/255, blue: 255/255, alpha: 0.6), for: .highlighted)
        editBtn.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        let testBtn = UIButton()
        testBtn.setTitle("测试此场景", for: .normal)
        testBtn.setTitleColor(UIColor(red: 0, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
        testBtn.setTitleColor(UIColor(red: 0, green: 122/255, blue: 255/255, alpha: 0.6), for: .highlighted)
        testBtn.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        let divider = UIView()
        

        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.alignment = .center
        stackView.addArrangedSubview(editBtn)
        stackView.addArrangedSubview(divider)
        stackView.addArrangedSubview(testBtn)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.backgroundColor = UIColor.blue
        view.addSubview(stackView)
        
        let bgView = UIView()
        stackView.insertSubview(bgView, at: 0)
        bgView.backgroundColor = UIColor.white
        bgView.translatesAutoresizingMaskIntoConstraints = false
        bgView.leadingAnchor.constraint(equalTo: stackView.leadingAnchor).isActive = true
        bgView.trailingAnchor.constraint(equalTo: stackView.trailingAnchor).isActive = true
        bgView.topAnchor.constraint(equalTo: stackView.topAnchor).isActive = true
        bgView.bottomAnchor.constraint(equalTo: stackView.bottomAnchor).isActive = true
        bgView.layer.cornerRadius = 10
        
        divider.translatesAutoresizingMaskIntoConstraints = false
        divider.leadingAnchor.constraint(equalTo: stackView.leadingAnchor).isActive = true
        divider.trailingAnchor.constraint(equalTo: stackView.trailingAnchor).isActive = true
        divider.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        divider.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2)
        
        editBtn.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        editBtn.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10).isActive = true
        testBtn.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        testBtn.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10).isActive = true
        
        stackView.bottomAnchor.constraint(equalTo: deleteBtn.topAnchor, constant: -20).isActive = true
        stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
}
