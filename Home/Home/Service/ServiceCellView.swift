//
//  ServiceCellView.swift
//  home
//
//  Created by Yun Zeng on 2019/1/28.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import UIKit

class ServiceCellView: UIView {

    // 图标位置
    static let iconSize = CGRect(x: 0, y: 0, width: 72, height: 50)
    
    // 图标
    var iconView: UIView = UIView(frame: ServiceCellView.iconSize) {
        didSet {
            oldValue.removeFromSuperview()
            self.addSubview(self.iconView)
        }
    }
    
    // 房间
    public var room: String = "默认房间" {
        didSet {
            self.roomLabel.text = self.room
        }
    }
    
    private lazy var roomLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 11, y: 49, width: 90, height: 18))
        label.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.medium)
        label.textColor = UIColor.darkGray
        label.text = self.room
        return label
    } ()
    
    // 服务名
    public var name: String = "请命名" {
        didSet {
            self.nameLabel.text = self.name
        }
    }
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 11, y: 65, width: 90, height: 18))
        label.frame = CGRect(x: 11, y: 65, width: 90, height: 18)
        label.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.medium)
        label.textColor = UIColor(red: 98 / 255, green: 98 / 255, blue: 98 / 255, alpha: 1)
        label.text = self.name
        return label
    } ()
    
    // 状态描述
    public var desc: String = "正在更新" {
        didSet {
            self.descLabel.text = self.desc
        }
    }
    
    private lazy var descLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 11, y: 81, width: 90, height: 18))
        label.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.medium)
        label.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        label.text = self.desc
        return label
    } ()
    
    // 背景模糊
    private lazy var blurView: UIVisualEffectView = {
        return UIVisualEffectView(effect: nil)
    }()
    
    private func addBlurView(style: UIBlurEffect.Style) {
        self.backgroundColor = .clear
        self.blurView.effect = UIBlurEffect(style: .light)
        self.blurView.translatesAutoresizingMaskIntoConstraints = false
        self.blurView.frame = self.frame
        self.blurView.clipsToBounds = true
        self.blurView.backgroundColor = UIColor(red: 200 / 255, green: 200 / 255, blue: 200 / 255, alpha: 0.7)
        self.blurView.layer.cornerRadius = 13
        self.blurView.layer.opacity = 1
    }
    
    private func removeBlurView() {
        self.backgroundColor = UIColor.white
        self.blurView.layer.opacity = 0
    }
    
    // 服务状态
    var state = Service.State.offline {
        didSet {
            if state == .highlight {
                self.removeBlurView()
            } else {
                self.addBlurView(style: .regular)
            }
        }
    }
    
    // 页面初始化
    func initialize() {
        self.addSubview(self.iconView)
        self.addSubview(self.roomLabel)
        self.addSubview(self.nameLabel)
        self.addSubview(self.descLabel)
        self.insertSubview(self.blurView, at: 0)
        if state == .highlight {
            self.removeBlurView()
        } else {
            self.addBlurView(style: .light)
        }
        self.layer.cornerRadius = 13
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

