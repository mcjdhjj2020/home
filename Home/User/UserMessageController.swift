//
//  UserMessageController.swift
//  home
//
//  Created by Yun Zeng on 2019/8/6.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import UIKit

class UserMessageController: UIViewController {
    
    private lazy var customBar: CustomNavigationBar = {
        let bar = CustomNavigationBar()
        bar.title = "消息通知"
        bar.subtitle = "2019-03-06"
        bar.icon = UIImage(named: "user-message")
        return bar
    } ()
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(hex: "dedede")
        
        view.addSubview(self.customBar)
        customBar.translatesAutoresizingMaskIntoConstraints = false
        customBar.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        customBar.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        customBar.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        customBar.cancel = {
            self.dismiss(animated: true, completion: nil)
        }
                  
    }
}

