//
//  UserSettingCell.swift
//  home
//
//  Created by Yun Zeng on 2019/8/6.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import UIKit

class UserSettingCellView: UIView {
    @IBInspectable var icon: String = "" {
        didSet {
            self.iconImage.image = UIImage(named: icon)
        }
    }
    
    @IBInspectable var title: String = "" {
        didSet {
            let attributes: [NSAttributedString.Key : Any] = [
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17, weight: .regular),
                NSAttributedString.Key.foregroundColor: UIColor(red: 0, green: 0, blue: 0, alpha: 1),
                NSAttributedString.Key.kern: -0.41]
            self.titleLable.attributedText = NSAttributedString(string: title, attributes: attributes)
        }
    }
    
    private lazy var iconImage: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "user-family")
        return view
    }()
    
    private lazy var titleLable: UILabel = {
        let label = UILabel()
        return label
    }()
    
    
    func initialize()  {
        self.addSubview(iconImage)
        self.addSubview(titleLable)

        iconImage.translatesAutoresizingMaskIntoConstraints = false
        iconImage.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15).isActive = true
        iconImage.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        iconImage.heightAnchor.constraint(equalToConstant: 35).isActive = true
        iconImage.widthAnchor.constraint(equalToConstant: 35).isActive = true
        
        titleLable.translatesAutoresizingMaskIntoConstraints = false
        titleLable.leadingAnchor.constraint(equalTo: iconImage.trailingAnchor, constant: 8).isActive = true
        titleLable.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        titleLable.heightAnchor.constraint(equalToConstant: 40).isActive = true
        titleLable.autoresizingMask = .flexibleWidth
        
        
        self.backgroundColor = UIColor.clear
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
}


class UserSettingCell: UITableViewCell {
    static let identifier = "UserSettingCell"
    
    var settingView: UserSettingCellView!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        settingView = UserSettingCellView(frame: .zero)
        self.contentView.addSubview(settingView)
        
        self.accessoryType = .disclosureIndicator
        settingView.translatesAutoresizingMaskIntoConstraints = false
        settingView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        settingView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        settingView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        settingView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        self.backgroundColor = UIColor.clear
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
