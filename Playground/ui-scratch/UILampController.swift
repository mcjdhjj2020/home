//
//  UILampController.swift
//  home
//
//  Created by Yun Zeng on 2019/9/23.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import UIKit


class UILampViewController: UIViewController {
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy private var progressView: ProgressView = {
        let view = ProgressView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: 343).isActive = true
        view.widthAnchor.constraint(equalToConstant: 123).isActive = true
        view.backgroundColor = UIColor.clear
        return view
    } ()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        self.view.addSubview(self.progressView)
        progressView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        progressView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        progressView.percentage = 20
    }
    
}

// MARK: 亮度控制
fileprivate class ProgressView: UIView {
    // 背景颜色
    let bgColor = UIColor(red: 0.957, green: 0.957, blue: 0.957, alpha: 1.000)
    // 填充颜色
    var fillColor = UIColor(red: 0.596, green: 0.984, blue: 0.698, alpha: 1.000) {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    var percentage: Int = 0 {
        didSet {
            self.height = Int(343 * self.percentage / 100)
        }
    }
    
    var onProgress: ((_ percentage: Int) -> Void)?
    
    private var height: Int = 0 {
        didSet {
            UIView.animate(withDuration: 0.5) {
                self.setNeedsDisplay()
            }
        }
    }
    
    override func draw(_ rect: CGRect) {
        // General Declarations
        let context = UIGraphicsGetCurrentContext()!
        // Group 3
        context.saveGState()
        context.beginTransparencyLayer(auxiliaryInfo: nil)

        // Clip Clip
        let clipPath = UIBezierPath()
        clipPath.move(to: CGPoint(x: 30, y: 343))
        clipPath.addLine(to: CGPoint(x: 94, y: 343))
        clipPath.addCurve(to: CGPoint(x: 124, y: 313), controlPoint1: CGPoint(x: 110.57, y: 343), controlPoint2: CGPoint(x: 124, y: 329.57))
        clipPath.addLine(to: CGPoint(x: 124, y: 30))
        clipPath.addCurve(to: CGPoint(x: 94, y: 0), controlPoint1: CGPoint(x: 124, y: 13.43), controlPoint2: CGPoint(x: 110.57, y: 0))
        clipPath.addLine(to: CGPoint(x: 30, y: 0))
        clipPath.addCurve(to: CGPoint(x: 0, y: 30), controlPoint1: CGPoint(x: 13.43, y: 0), controlPoint2: CGPoint(x: 0, y: 13.43))
        clipPath.addLine(to: CGPoint(x: 0, y: 313))
        clipPath.addCurve(to: CGPoint(x: 30, y: 343), controlPoint1: CGPoint(x: 0, y: 329.57), controlPoint2: CGPoint(x: 13.43, y: 343))
        clipPath.close()
        clipPath.usesEvenOddFillRule = true
        clipPath.addClip()

        // Rectangle Drawing
        let rectanglePath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: 124, height: 343))
        bgColor.setFill()
        rectanglePath.fill()

        // Rectangle 2 Drawing
        let rectangle2Path = UIBezierPath(rect: CGRect(x: 0, y: 343-self.height, width: 124, height: self.height))
        fillColor.setFill()
        rectangle2Path.fill()

        context.endTransparencyLayer()
        context.restoreGState()
    }
        
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let point = touches.first!.preciseLocation(in: self)
        self.moveTo(point: point)
        UIView.animate(withDuration: 0.1) {
            self.transform = CGAffineTransform(scaleX: 1.03, y: 1.03)
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let point = touches.first!.preciseLocation(in: self)
        self.moveTo(point: point)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let point = touches.first!.preciseLocation(in: self)
        self.moveTo(point: point)
        UIView.animate(withDuration: 0.1) {
            self.transform = .identity
        }
    }
    
    func moveTo(point: CGPoint) {
        if point.y > 0 && point.y < 343 {
            self.height = 343 - Int(point.y)
        } else if point.y < 0 {
            self.height = 343
        } else {
            self.height = 0
        }
        let percentage = Int(self.height * 100 / 343)
        self.onProgress?(percentage)
    }
}
